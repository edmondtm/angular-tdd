// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBm4Q6wM1Qe_eqQD75nv9ZK32Msk00tpHs',
    authDomain: 'unit-testing-5f196.firebaseapp.com',
    databaseURL: 'https://unit-testing-5f196.firebaseio.com',
    projectId: 'unit-testing-5f196',
    storageBucket: 'unit-testing-5f196.appspot.com',
    messagingSenderId: '320236456489'
  }
};
