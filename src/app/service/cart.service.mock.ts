import { CartService } from './cart.service';
import { Observable, Subject, of } from 'rxjs';
import { BookModel } from '../model/book.model';
import * as faker from 'faker';


export let CartList: BookModel[] = [];

for (let i = 0; i < 10; i++) {
    CartList.push(new BookModel(
        faker.image.image(),
        faker.lorem.sentence(),
        faker.lorem.sentence(),
        +faker.commerce.price(),
        faker.random.number()
    ));
}

export class CartServiceMock {

    private emitAddToCart = new Subject<any>();
    addEmitted$ = this.emitAddToCart.asObservable();

    query() {
        return of('CartList');
    }

    add(item: BookModel): BookModel {
        return item;
    }

    emitChange(book: BookModel) {
        this.emitAddToCart.next(book);
    }

}

