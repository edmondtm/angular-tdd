import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { CartService } from './cart.service';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule, AngularFirestore } from 'angularfire2/firestore';
import { CartList, CartServiceMock } from './cart.service.mock';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { BookModel } from '../model/book.model';

const AngularFirestoreMock = {
  collection: function(param: any) {
    return {
      valueChanges: function() { return of(CartList); },
      add: function(item) { return item; }
    };
  }
};

describe('CartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireModule.initializeApp(environment.firebase),
        AngularFirestoreModule
      ],
      providers: [
        CartService,
        { provide: AngularFirestore, useValue: AngularFirestoreMock }
      ]
    });
  });

  it('should be created', inject([CartService], (service: CartService) => {
    expect(service).toBeTruthy();
  }));

  it('should have add method defined', inject([CartService], (service: CartService) => {
    expect(service.add).toBeTruthy();
  }));

  xit('should have add method working', inject([CartService], fakeAsync((service: CartService) => {
    const items = CartService;
    const itemsAdded = service.add(items);
    tick();
    expect(itemsAdded).toBe(items);
  })));

  it('should have query method defined', inject([CartService], (service: CartService) => {
    expect(service.query).toBeTruthy();
  }));

  it('should have working query method', inject([CartService], fakeAsync((service: CartService) => {
    const books$ = service.query();
    let response;

    books$.subscribe((items) => {
      response = items;
      console.log(response);
    });

    tick();

    expect(response).toBe(CartList);
  })));

  it('should have emitChange method defined', inject([CartService], (service: CartService) => {
    expect(service.emitChange).toBeTruthy();
  }));

});
