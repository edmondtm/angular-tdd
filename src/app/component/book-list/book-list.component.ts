import { Component, OnInit } from '@angular/core';
import { BookModel } from '../../model/book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  books: BookModel[];

  constructor() { 
    this.books = BookModel.query()
  }

  ngOnInit() {
  }

}
