import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BookModel } from '../../model/book.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input() book: BookModel;
  @Output() addToCart: EventEmitter<BookModel> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  votesCounter(): number {
    return this.book.upvotes;
  }

  upvote(): number {
    return this.book.upvotes++;
  }

  sendToCart() {
    this.addToCart.emit(this.book);
  }

}
