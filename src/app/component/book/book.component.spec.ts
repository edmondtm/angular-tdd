import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CartService } from '../../service/cart.service';
import { CartServiceMock } from '../../service/cart.service.mock';

import * as faker from 'faker';

import { BookComponent } from './book.component';
import { BookModel } from '../../model/book.model';

describe('BookComponent', () => {
  let component: BookComponent;
  let fixture: ComponentFixture<BookComponent>;
  let book: BookModel;
  let nativeElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookComponent ],
      providers: [
        { provide: CartService, useClass: CartServiceMock}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookComponent);
    component = fixture.componentInstance;
    book = new BookModel(
      faker.image.image(),
      faker.lorem.words(),
      faker.lorem.paragraph(),
      1234.55,
      0
    );
    component.book = book;
    fixture.detectChanges();
    nativeElement = fixture.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display image', () => {
    const image = nativeElement.querySelector('.book-image').getAttribute('src');
    expect(image).toEqual(book.image);
  });

  it('should display title', () => {
    const title = nativeElement.querySelector('.book-title').innerHTML;
    expect(title).toEqual(book.title);
  });

  it('should display description', () => {
    const description = nativeElement.querySelector('.book-description').innerHTML;
    expect(description).toEqual(book.descripiton);
  });

  it('should display price', () => {
    const price = nativeElement.querySelector('.book-price').innerHTML;
    expect(price).toEqual('$1,234.55');
  });

  it('should set correct number of upvotes', () => {
    const votes = component.votesCounter();
    expect(component.votesCounter()).toEqual(votes);
    expect(component.votesCounter()).toBeLessThan(votes + 1);
    expect(component.votesCounter()).toBeGreaterThan(votes - 1);
  });

  it('upvote invokes the component function', () => {
    const spy = spyOn(component, 'upvote');
    const button = nativeElement.querySelector('button.upvote');
    button.dispatchEvent(new Event('click'));
    expect(spy).toHaveBeenCalled();
  });

  it('shoule emmit addToCart event', (done) => {
    component.addToCart.subscribe((e) => {
      expect(e).toEqual(component.book);
      done();
    });
    component.sendToCart();
  });

  it('should call sendToCart when "Add To Cart" button is clicked', () => {
    const spy = spyOn(component, 'sendToCart');
    const button = nativeElement.querySelector('button.addToCart');
    button.dispatchEvent(new Event('click'));
    expect(spy).toHaveBeenCalled();
  });

});
