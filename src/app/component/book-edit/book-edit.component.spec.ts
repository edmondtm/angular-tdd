import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { BookEditComponent } from './book-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import * as faker from 'faker';
import { BookModel } from '../../model/book.model';

describe('BookEditComponent', () => {
  let component: BookEditComponent;
  let fixture: ComponentFixture<BookEditComponent>;
  let nativeElement: HTMLElement;

  // let image = faker.image.image();
  // let title = faker.lorem.word();
  // let description = faker.lorem.sentence();
  // let price = faker.commerce.price();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookEditComponent ],
      imports: [ FormsModule, ReactiveFormsModule, RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    nativeElement = fixture.nativeElement;
  });

  afterEach(() => {
    if (component.book) {
      component.book.destroy();
    }
  });

  function FillTheForm(image, title, description, price) {
    component.bookEditForm.controls['image'].setValue(image);
    component.bookEditForm.controls['title'].setValue(title);
    component.bookEditForm.controls['description'].setValue(description);
    component.bookEditForm.controls['price'].setValue(price);
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have blank fields initially', () => {
    expect(component.bookEditForm.value).toEqual({
      image: '',
      title: '',
      description: '',
      price: ''
    });
  });

  it('submit button not clickable if form is invalid', () => {
    fakeAsync(() => {
      const spy = spyOn(component, 'submitReactiveForm');
      FillTheForm('', '', faker.lorem.sentence(), faker.commerce.price());
      const button = nativeElement.querySelector('#reactiveSubmitButton');
      button.dispatchEvent(new Event('click'));
      expect(spy).not.toHaveBeenCalled();
      expect(button.hasAttribute('disabled')).toBe(true);

    });
  });

  it('submit button submit form if valid', () => {
    fakeAsync(() => {
      const spy = spyOn(component, 'submitReactiveForm').and.callThrough();
      FillTheForm(
        faker.image.image(),
        faker.lorem.sentence(),
        faker.lorem.sentence(),
        faker.commerce.price());
      const button = nativeElement.querySelector('#reactiveSubmitButton');
      button.dispatchEvent(new Event('click'));
      expect(spy).toHaveBeenCalled();
      expect(button.hasAttribute('disabled')).toBe(false);
      const bookFromStorage = BookModel.find(component.book.title);
      expect(bookFromStorage).toEqual(this.books);
    });
  });



});
