import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BookModel } from '../../model/book.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  bookEditForm: FormGroup;
  book: BookModel;
  activeForm: string = 'reactive';

  constructor(fb: FormBuilder, private route: ActivatedRoute) { 
    this.bookEditForm = fb.group({
      title: ['', Validators.required],
      image: ['', Validators.required],
      description: [''],
      price: ['']
    });
    route.params.subscribe( res => {
      if(this.book == null) {
        this.book = new BookModel('', '', '', 0, 0);
      }
    })
  }

  ngOnInit() {
  }

  submitReactiveForm(): void {
    let bookData = this.prepareSaveBook();
    this.book = new BookModel(
      bookData.image,
      bookData.title,
      bookData.descripiton,
      bookData.price,
      bookData.upvotes,
    )
    this.book.save();
  }

  prepareSaveBook(): BookModel {
    const formModel = this.bookEditForm.value;
    return formModel;
  }

}
