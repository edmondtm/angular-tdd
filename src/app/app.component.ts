import { Component } from '@angular/core';
import { BookModel } from './model/book.model';
import { CartList } from './service/cart.service.mock';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  cart: Array<BookModel> = CartList;

  public book: BookModel = new BookModel(
    'https://upload.wikimedia.org/wikipedia/commons/1/1d/Tom_Sawyer_1876_frontispiece.jpg',
    'Tom Sawyer',
    `Tom Sawyer is a boy of about 12 years of age, who resides in the fictional town of St. Petersburg,
     Missouri, in about the year 1845. Tom Sawyer's best friends include Joe Harper and Huckleberry Finn.
     In The Adventures of Tom Sawyer, Tom's infatuation with classmate Becky Thatcher is apparent as he
     tries to intrigue her with his strength, boldness, and handsome looks.`,
    15,
    0
  );

  addToCart(book) {
    this.cart.push(book);
  }

}
