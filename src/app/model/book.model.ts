export interface BookInterface {
    image: string,
    title: string,
    descripiton: string,
    price: number,
    upvotes?: number
}
export class BookModel implements BookInterface {
    constructor(
        public image: string,

        public title: string,
        public descripiton: string,
        public price: number,
        public upvotes: number
    ){}

    destroy(): boolean {
        let books: Array<BookModel> = JSON.parse(localStorage.getItem('books') || '[]');
        books.forEach((item, index) => {
            if(item.title == this.title) {
                books.splice(index, 1);
                localStorage.setItem('books', JSON.stringify(books));
            } 
        });
        return null;
    }


    save(): boolean {
        let books: Array<BookModel> = JSON.parse(localStorage.getItem('books') || '[]');
        books.forEach((item, index) => {
            if(item.title == this.title) books.slice(index, 1);
        });
        books.push(this);
        localStorage.setItem('books', JSON.stringify(books));
        return true
    }

    public static find(title: string): BookModel | null {
        let books: Array<BookModel> = JSON.parse(localStorage.getItem('books') || '[]');
        for (let book of books) {
            if(book.title === title) {
                return new BookModel(
                    book.image,
                    book.title,
                    book.descripiton,
                    book.price,
                    book.upvotes) }
        }
        return null            
        
    }

    public static query() {
        let books: Array<BookModel> = JSON.parse(localStorage.getItem('books') || '[]');
        let BookModels: BookModel[] = [];
        for (let book of books) {
            BookModels.push(
                new BookModel(book.image, book.title, book.descripiton, book.price, book.upvotes)
            );
        }
        return BookModels;
    }

}