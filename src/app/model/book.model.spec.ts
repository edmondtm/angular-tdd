import { BookModel, BookInterface} from './book.model';
import * as faker from 'faker';

describe('BookModel', () => {
    let image: string;
    let title: string;
    let descripiton: string;
    let price: number;
    let upvotes: number;
    let book: BookModel;

    beforeEach(() => {
        image = faker.image.image();
        title = faker.lorem.words();
        descripiton = faker.lorem.sentence();
        price = +faker.commerce.price();
        upvotes = faker.random.number();
        this.book = new BookModel(image, title, descripiton, price, upvotes);

        let storage = {};

        spyOn(window.localStorage, 'getItem').and.callFake((key: string): string => {
            return storage[key] || null;
        })

        spyOn(window.localStorage, 'removeItem').and.callFake((key: string): void => {
            delete storage[key];
        })

        spyOn(window.localStorage, 'setItem').and.callFake((key: string, value: string): string => {
            return storage[key] = value + '';
        })

        spyOn(window.localStorage, 'clear').and.callFake((): void => {
            storage = {};
        })
    })

    afterEach(() => localStorage.clear())

    it('has a valid model', () => {
        expect(this.book.image).toEqual(image);
        expect(this.book.title).toEqual(title);
        expect(this.book.descripiton).toEqual(descripiton);
        expect(this.book.price).toEqual(price);
        expect(this.book.upvotes).toEqual(upvotes);
    })

    it('has localStorage working', () => {
        expect(localStorage.setItem('key','value')).toBeFalsy;
        expect(localStorage.getItem('key')).toBe('value');
    })

    it('has the find and save method working', () => {
        this.book.save();
        let bookStorage: BookModel = BookModel.find(this.book.title);
        expect(bookStorage).toEqual(this.book);
    })

    it('has the destroy method working', () => {
        this.book.save();
        this.book.destroy();
        let bookFromStorage: BookModel = BookModel.find(this.book.title);
        console.log(bookFromStorage)
        expect(bookFromStorage).toBeTruthy;
        expect(bookFromStorage).toEqual(null);
    })
});

